<?php

declare(strict_types = 1);

namespace Drupal\config_duplicator\Services;

/**
 * Service for duplicating existing Drupal config.
 */
interface ConfigDuplicatorInterface {

  /**
   * Duplicate the config files.
   *
   * @param string $source_machine_name
   *   The source config machine name.
   * @param string $target_machine_name
   *   The target config machine name.
   */
  public function duplicate($source_machine_name, $target_machine_name);

}
