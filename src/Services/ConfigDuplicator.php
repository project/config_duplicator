<?php

declare(strict_types = 1);

namespace Drupal\config_duplicator\Services;

use Drupal\Core\Config\CachedStorage;
use Drupal\Component\Uuid\UuidInterface;

/**
 * Service for duplicating existing Drupal config.
 */
class ConfigDuplicator implements ConfigDuplicatorInterface {

  /**
   * The config storage service.
   *
   * @var \Drupal\Core\Config\CachedStorage
   */
  protected CachedStorage $configStorage;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * The source config machine name.
   *
   * @var string
   */
  protected string $sourceMachineName;

  /**
   * The target config machine name.
   *
   * @var string
   */
  protected string $targetMachineName;

  /**
   * The service constructor.
   *
   * @param \Drupal\Core\Config\CachedStorage $configStorage
   *   The config storage service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID service.
   */
  public function __construct(CachedStorage $configStorage, UuidInterface $uuid_service) {
    $this->configStorage = $configStorage;
    $this->uuidService = $uuid_service;
  }

  /**
   * Returns the config that need to be duplicated.
   */
  protected function findConfig(): array {
    return preg_grep('/' . $this->sourceMachineName . '/i', $this->configStorage->listAll());
  }

  /**
   * Replaces the old machine name with the new one in the config values.
   */
  protected function replaceMachineName(array $source): array {
    foreach ($source as $config_name => $config_data) {
      $source[$config_name] =
        is_array($config_data) ? $this->replaceMachineName($config_data) :
          (
            is_string($config_data) ? str_replace($this->sourceMachineName, $this->targetMachineName, $config_data) :
            $config_data
          );
    }
    return $source;
  }

  /**
   * {@inheritdoc}
   */
  public function duplicate($source_machine_name, $target_machine_name) {
    $this->sourceMachineName = $source_machine_name;
    $this->targetMachineName = $target_machine_name;

    $config = $this->findConfig();
    foreach ($config as $config_name) {
      $config_data = $this->configStorage->read($config_name);
      $config_data = $this->replaceMachineName($config_data);
      $config_data['uuid'] = $this->uuidService->generate();
      $machine_name = str_replace($this->sourceMachineName, $this->targetMachineName, $config_name);
      $this->configStorage->write($machine_name, $config_data);
    }
  }

}
