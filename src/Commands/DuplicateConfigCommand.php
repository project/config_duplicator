<?php

declare(strict_types = 1);

namespace Drupal\config_duplicator\Commands;

use Drupal\config_duplicator\Services\ConfigDuplicatorInterface;

/**
 * Drush Command class for duplicating existing Drupal config.
 */
class DuplicateConfigCommand {

  /**
   * The config duplicator service.
   *
   * @var \Drupal\config_duplicator\Services\ConfigDuplicatorInterface
   */
  protected $configDuplicator;

  /**
   * The config duplicator drush command.
   *
   * @param \Drupal\config_duplicator\Services\ConfigDuplicatorInterface $config_duplicator
   *   The config duplicator service.
   */
  public function __construct(ConfigDuplicatorInterface $config_duplicator) {
    $this->configDuplicator = $config_duplicator;
  }

  /**
   * Duplicate  set of config files.
   *
   * @command config_duplicator:duplicate
   * @aliases cdup
   * @bootstrap full
   *
   * @usage config_duplicator:duplicate page article
   *   Duplicates all config with page in the name to new config with article in
   *   the name.
   */
  public function duplicate(string $source_machine_name, string $target_machine_name,) {
    $this->configDuplicator->duplicate($source_machine_name, $target_machine_name);
  }

}
