# Config Duplicator

The Config Duplicator module provides a Drush command to duplicate multiple config entities containing a keyword to a new set of config entities. This is useful if you want to quickly generate configuration based on a set of existing config entities.

## Features

* Copy all config entities containing a keyword
* Generate a new set of config entities with new UUIDs
* Replace all instances of the keyword by a provided machine name in the new set of config entities

## Usage
After installing the module you can use the drush command like this:

```
drush cdup keyword machine_name
drush cex -y
```


* Replace `keyword` with the keyword used in the name of all config entities you want to duplicate.
* Replace `machine_name` with the machine name you want to use in the new config entities. This will replace the keyword.
* Run `drush cex` to export the newly generated config.
* Double check the generated config. Because the keyword is replaced by the new machine name it may cause some unintended issues if the keyword is too broad or part of another keyword (e.g. "page" and "homepage").


It is recommended to install this module as a dev requirement.
